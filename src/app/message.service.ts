import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  // キャッシュのしくみ
  // messageesという配列を作り、messageを格納？
  messages: string[] = [];
  add(message: string) {
    this.messages.push(message);
  }
  // 以下メソッドでmessagesの中身をクリア
  clear() {
    this.messages = [];
  }
  // constructor() { }
}
