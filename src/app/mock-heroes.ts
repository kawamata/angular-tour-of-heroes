import { Hero } from './hero';

export const HEROES: Hero[] = [
  { id: 11, name: 'Chai' },
  { id: 12, name: 'Snow' },
  { id: 13, name: 'Oji-chan' },
  { id: 14, name: 'Polico' },
  { id: 15, name: 'JW' },
  { id: 16, name: 'Myan-myan' },
  { id: 17, name: 'Lama' },
  { id: 18, name: 'Athena' },
  { id: 19, name: 'Yuki-chan' },
  { id: 20, name: 'Saku-chan' }
];
