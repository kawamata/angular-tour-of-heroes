import { Component, OnInit } from '@angular/core';

// import { Hero } from '../hero';
// import { HEROES } from '../mock-heroes';
import { HeroService } from '../hero.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {
  // hero: Hero = {
  //   id: 1,
  //   name: 'Pochi'
  // }
  // heroes = HEROES;
  // heroes: Hero[];
  constructor(private heroService: HeroService) { }
  getHeroes(): void {
    // 以下の書き方は、実際のアプリ
    // （モックデータではなくリモートサーバのデータを扱う場合）には
    // 別の書き方になるので要注意
    // this.heroes = this.heroService.getHeroes();

    this.heroService.getHeroes().subscribe(heroes => this.heroes = heroes)
  }

  // selectedHero: Hero;
  onSelect(hero: Hero): void {
    this.selectedHero = hero;
  }

  ngOnInit() {
    this.getHeroes();
  }

}
